package com.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var first: Int
        var second: Int
        var operation = ""

        var value: Any?


        btn_1.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "1"
            } else value = "${value}1"
            editText.text = value.toString()
        }

        btn_2.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "2"
            } else value = "${value}2"
            editText.text = value.toString()
        }

        btn_3.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "3"
            } else value = "${value}3"
            editText.text = value.toString()
        }

        btn_4.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "4"
            } else value = "${value}4"
            editText.text = value.toString()
        }

        btn_5.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "5"
            } else value = "${value}5"
            editText.text = value.toString()
        }

        btn_6.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "6"
            } else value = "${value}6"
            editText.text = value.toString()
        }

        btn_7.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "7"
            } else value = "${value}7"
            editText.text = value.toString()
        }

        btn_8.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "8"
            } else value = "${value}8"
            editText.text = value.toString()
        }

        btn_9.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "9"
            } else value = "${value}9"
            editText.text = value.toString()
        }

        but_0.setOnClickListener{
            value = editText.text.toString()
            if (value == "0"){
                value = "0"
            } else value = "${value}0"
            editText.text = value.toString()
        }


        btn_sum.setOnClickListener{
            operation = btn_sum.text.toString()
            value = editText.text.toString()
            val pad: Any?

            if (value == "0")
                pad = "0 $operation "
            else
            pad = "$value $operation "

            editText.text = pad

            btn_del.isEnabled = false
            btn_multi.isEnabled = false
            btn_raz.isEnabled = false

        }

        btn_del.setOnClickListener{
            operation = btn_del.text.toString()
            value = editText.text.toString()
            val pad: Any?

            if (value == "")
                pad = "0 $operation "
            else
                pad = "$value $operation "

            editText.text = pad

            btn_sum.isEnabled = false
            btn_multi.isEnabled = false
            btn_raz.isEnabled = false
        }

        btn_raz.setOnClickListener{
            operation = btn_raz.text.toString()
            value = editText.text.toString()
            val pad: Any?

            if (value == "")
                pad = "0 $operation "
            else
                pad = "$value $operation "

            editText.text = pad

            btn_sum.isEnabled = false
            btn_multi.isEnabled = false
            btn_del.isEnabled = false
        }

        btn_multi.setOnClickListener{
            operation = btn_multi.text.toString()
            value = editText.text.toString()
            val pad: Any?

            if (value == "")
                pad = "0 $operation "
            else
                pad = "$value $operation "

            editText.text = pad

            btn_sum.isEnabled = false
            btn_del.isEnabled = false
            btn_raz.isEnabled = false
        }

        btn_c.setOnClickListener{
            editText.text = "0"

            btn_sum.isEnabled = true
            btn_multi.isEnabled = true
            btn_raz.isEnabled = true
            btn_del.isEnabled = true
        }

            btn_r.setOnClickListener{
                value = editText.text.toString()

/*                if (value.toString().substringBefore(" $operation").isNotEmpty()) {
                    first.toDouble().toInt()
                }*/

                first = value.toString().substringBefore(" $operation").toDouble().toInt()

                if (value.toString().substringAfter("$operation ").isNotEmpty()) {
                    second = value.toString().substringAfter("$operation ").toInt()
                } else {
                    Toast.makeText(this, getString(R.string.empty), Toast.LENGTH_LONG).show()
                    second = 0
                }

                val result = if (operation == "+") {
                    Sum(first, second)
                } else if (operation == "-"){
                    Raz(first, second)
                } else if (operation == "*"){
                    Multi(first, second)
                } else Del(first.toDouble(), second.toDouble())

                editText.text = result.toString()

                btn_sum.isEnabled = true
                btn_multi.isEnabled = true
                btn_raz.isEnabled = true
                btn_del.isEnabled = true

            }
    }

    fun Sum(a: Int, b: Int): Int {
        val res = a + b
        return res
    }
    fun Raz(a: Int, b: Int): Int {
        val res = a - b
        return res
    }
    fun Multi(a: Int, b: Int): Int {
        val res = a * b
        return res
    }
    fun Del(a: Double, b: Double): Any? {
        val res: Any?
        if (a.equals(0.0)){
            res = 0.0
        } else if (b.equals(0.0)) {
            res = getString(R.string.infinity)
        } else if (a.equals(0.0) && b.equals(0.0)) {
            res = 0.0
        } else {
            res = a / b
        }
        return res
    }
}
